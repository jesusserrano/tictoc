<div>
    <div class="form-group row col-6">
        <label for="nombre">Nombre</label>
        <input type="text" class="form-control col-6" id="nombre" wire:model="nombre"><br>
        <small>{{ $nombre }}</small>
    </div>

    <div class="form-group row col-6">
        <label for="mensaje">Mensaje</label>
        <input type="text" class="form-control col-6" id="mensaje" wire:model="mensaje"><br>
        <small>{{ $mensaje }}</small>
    </div>

    <div class="form-group row col-6">
        <button class="btn btn-success" wire:click="enviardata">guardar</button>
    </div>


        <div style="position: absolute;" class="alert alert-primary collapse" role="alert" id="aviso">
           save!
          </div>


    <script>
            window.livewire.on('mensajeEnviado',function () {
                $('#aviso').fadeIn('slow');
                setTimeout(() => {
                    $('#aviso').fadeOut("slow");
                }, 3000);
            })
    </script>


</div>
